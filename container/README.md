This is a sample container to use meta-rpm in a container.

Build it using:

```
$ podman build -t meta-rpm .
```

Alternatively, you can pull a pre-build version like so:

```
$ podman pull docker.io/alexl/meta-rpm
```

Then run it like:

```
$ podman run -t -i --device=/dev/fuse:rwm --device=/dev/kvm:rwm -v buildvolume:/data meta-rpm
```

This will create a new volume called `buildvolume` if it didn't exist yet, and
start a container with it in /data. If the volume is empty it will tell
you to run the command `initial-setup` which will set up the volume and enter
a build environment. If the volume was previously set up then it will
automatically enter the yocto build environment.

Once you are in the build environment you can run bitbake to trigger
builds like in a regular yocto setup.

For convenience, any arguments passed to the container run comman will
be run as a shell command inside the build env. For instance you can
directly start a build like so:

```
$ podman run -t -i --device=/dev/fuse:rwm --device=/dev/kvm:rwm -v buildvolume:/data meta-rpm bitbake rpmbased-test-image
```

Note that the volume created for the build data (named `buildvolume`
in above examples) can get rather big, so make sure you delete it with
the `podman volume` commands when you no longer need it.

Note that the default qemu network method is tun device based which is not working
in the container, so when you run `runqemu` in the container, pass the argument
`slirp` to use a non-privileged network method intead.
