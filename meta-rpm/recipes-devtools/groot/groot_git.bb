require groot.inc

SRC_URI = "git://gitlab.com/fedora-iot/groot.git;branch=main;protocol=https"

SRCREV = "225e6366b720308209fd74240382172cf838c71d"

S = "${WORKDIR}/git"
PV = "0.1+git${SRCPV}"
