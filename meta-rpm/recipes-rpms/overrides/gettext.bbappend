post_rpm_install_append_class-native() {
    create_wrapper ${D}${bindir}/msgfmt \
                GETTEXTDATADIR="${STAGING_DATADIR_NATIVE}/gettext-${PV}/"
}

PROVIDES_append_class-native = " gettext-minimal-native"
