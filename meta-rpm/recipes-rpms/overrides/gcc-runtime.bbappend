PROVIDES += "virtual/${TARGET_PREFIX}compilerlibs"

rpmbased_post_rpm_install_append() {
  # Make sure g++ finds libstdc++.so.6
  ln -s libstdc++.so.6 ${D}${libdir}/libstdc++.so
}
