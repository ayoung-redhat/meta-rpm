# This is used for binutils-native, the cross linker is in binutils-cross-host.bb

rpmbased_post_rpm_install_append() {
  # We only want the binaries and the versioned libs in -native, otherwise it conflicts with -cross
  rm -rf ${D}${includedir} \
         ${D}${libdir}/libopcodes.so \
         ${D}${libdir}/libbfd.so \
         ${D}${libdir}/*.a \
         ${D}${datadir}

  # Rename libraries (and dependencies) to avoid conflict with binutils-cross
  for p in ${D}${libdir}/*.so; do
      l=$(basename $p)
      l2=$(echo $l | sed 's/lib\([[:alnum:]]*\)-/lib\1-rh-/')
      mv $p $(dirname $p)/$l2
      patchelf --replace-needed $l $l2 ${D}${bindir}/* ${D}${libdir}/*.so
  done

  # Resolve alternatives statically
  ln -s ld.bfd ${D}${bindir}/ld
}
