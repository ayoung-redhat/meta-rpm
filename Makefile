IMAGES_DIR := meta-rpm/recipes-core/images
IMAGES = $(basename $(notdir $(wildcard $(IMAGES_DIR)/*.bb)))
IMAGE_RULES = $(addprefix build-,$(IMAGES))

.PHONY: all recipes build-sample-images crossbuild-sample-images

.NOTPARALLEL: # The sample images don't build in parallel

TOPDIR=.
BINDIR=$(TOPDIR)/bin

all: recipes

recipes meta-rpm/recipes-rpms/generated:
	rm -rf $(TOPDIR)/meta-rpm/recipes-rpms/generated
	mkdir $(TOPDIR)/meta-rpm/recipes-rpms/generated
	ln -s ../overrides/files $(TOPDIR)/meta-rpm/recipes-rpms/generated/files
	$(BINDIR)/generate-recipes.py \
	   --mapping $(TOPDIR)/meta-rpm/recipes-rpms/overrides/package-mapping.yaml \
	   --dnfdir $(TOPDIR)/meta-rpm/recipes-rpms/overrides/dnf \
	   --destdir $(TOPDIR)/meta-rpm/recipes-rpms/generated/ \
	   --licenses $(TOPDIR)/meta-rpm/recipes-rpms/overrides/license_dict.json

sample-image-downloads:
	mkdir -p sample-image-downloads

sample-image-builddir: sample-image-downloads
	bash -c ". oe-init-build-env sample-image-builddir" # Create initial config
	echo 'DL_DIR = "$(shell pwd)/sample-image-downloads"' >> sample-image-builddir/conf/local.conf # Share dl dirs
	if [ `uname -m` == "x86_64" ]; then \
	   echo 'MACHINE = "qemux86-64"' >> sample-image-builddir/conf/local.conf; \
	fi
	if [ `uname -m` == "aarch64" ]; then \
	   echo 'MACHINE = "qemuarm64"' >> sample-image-builddir/conf/local.conf; \
	fi

build-%: $(IMAGES_DIR)/%.bb meta-rpm/recipes-rpms/generated sample-image-builddir
	@echo "Building $@"
	bash -c ". oe-init-build-env sample-image-builddir; bitbake $(notdir $(basename $<))"

build-sample-images: $(IMAGE_RULES)

sample-image-crossdir: sample-image-downloads
	bash -c ". oe-init-build-env sample-image-crossdir" # Create initial config
	echo 'DL_DIR = "$(shell pwd)/sample-image-downloads"' >> sample-image-crossdir/conf/local.conf # Share dl dirs
	if [ `uname -m` == "aarch64" ]; then \
	   echo 'MACHINE = "qemux86-64"' >> sample-image-crossdir/conf/local.conf; \
	else \
	   echo 'MACHINE = "qemuarm64"' >> sample-image-crossdir/conf/local.conf; \
	fi

cross-build-%: $(IMAGES_DIR)/%.bb meta-rpm/recipes-rpms/generated sample-image-crossdir
	@echo "Building $@"
	bash -c ". oe-init-build-env sample-image-crossdir; bitbake $(notdir $(basename $<))"

crossbuild-sample-images: $(addprefix cross-,$(IMAGE_RULES))
